import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.video.ConverterFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Georg Plaz.
 */
public class VideoCapture {
    private boolean running = false;
    private long startTimeNano;
    private IMediaWriter writer;
    private Robot robot;
    private Rectangle screenBounds;
    private final Object CAPTURE_LOCK = new Object();
    private final ImageThread imageThread = new ImageThread();
    private final Queue<ImageNode> imageNodes = new LinkedList<>();

    public void start(File file, int x, int y, int width, int height) throws AWTException {
        synchronized (CAPTURE_LOCK) {
            running = true;
            robot = new Robot();
            screenBounds = new Rectangle(x, y, width, height);

            writer = ToolFactory.makeWriter(file.getPath());
            writer.addVideoStream(0, 0, ICodec.ID.CODEC_ID_MPEG4, screenBounds.width, screenBounds.height);
            startTimeNano = -1;
            imageThread.start();
        }
        System.out.println("started video capture to "+file.getAbsolutePath());

    }


    public void captureScreen() {
        capture(robot.createScreenCapture(screenBounds));
    }

    public void captureScreen(long timeElapsed, TimeUnit timeUnit) {
        capture(robot.createScreenCapture(screenBounds), timeElapsed, timeUnit);
    }

    public void capture(Container container) {
        capture(container, System.nanoTime() - startTimeNano, TimeUnit.NANOSECONDS);
    }

    public void capture(Container container, long timeElapsed, TimeUnit timeUnit) {
        BufferedImage image = new BufferedImage(screenBounds.width, screenBounds.height, BufferedImage.TYPE_3BYTE_BGR);
        container.paintComponents(image.getGraphics());
//        container.paint(image.getGraphics());
        capture(image, timeElapsed, timeUnit);
    }

    public void capture(BufferedImage image) {
        capture(image, System.nanoTime() - startTimeNano, TimeUnit.NANOSECONDS);
    }

    public void capture(BufferedImage image, long timeElapsed, TimeUnit timeUnit) {
        synchronized (CAPTURE_LOCK) {
            if (startTimeNano == -1) {
                startTimeNano = System.nanoTime();
            }
            synchronized (imageNodes) {
                imageNodes.add(new ImageNode(timeElapsed, timeUnit, image));
            }
            synchronized (imageThread) {
                imageThread.notify();
            }
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void close() {
        synchronized (CAPTURE_LOCK) {
            running = false;
        }
    }

    private class ImageNode {
        private long time;
        private TimeUnit timeUnit;
        private BufferedImage image;

        private ImageNode(long time, TimeUnit timeUnit, BufferedImage image) {
            this.time = time;
            this.timeUnit = timeUnit;
            this.image = image;
        }
    }

    private class ImageThread extends Thread {
        @Override
        synchronized public void run() {
            while (running) {
                while (imageNodes.isEmpty()) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        //ignore
                    }
                }
                ImageNode node;
                synchronized (imageNodes) {
                    node = imageNodes.poll();
                }
                ConverterFactory.convertToType(node.image, BufferedImage.TYPE_3BYTE_BGR);
                writer.encodeVideo(0, node.image, node.time, node.timeUnit);
                System.out.println("wrote image at time: "+node.time+" "+node.timeUnit);
            }
            try {
                writer.close();
                System.out.println("closed video writer");
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }

    }
}
