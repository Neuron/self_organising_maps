import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public abstract class PointMap1D<A extends Node1D> extends PointMap<A> {
    private List<A> nodes = new ArrayList<>();
    public void add(A node) {
        add(node, nodes.size());
    }

    public void add(A node, int index) {
        nodes.add(index, node);
    }

    public void add(double x, double y) {
        add(create(x, y), nodes.size());
    }

    public void add(double x, double y, int index) {
        nodes.add(index, create(x, y));
    }

    @Override
    public List<A> getNodes() {
        return nodes;
    }

    public abstract A create(double x, double y);
}
