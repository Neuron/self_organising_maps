import java.util.*;

/**
 * Created by Georg Plaz.
 */
public class PointCircle extends PointMap1D<Node1D> {
    private Map<Node1D, Integer> positions = new HashMap<>();

    public void finish() {
        Iterator<Node1D> iterator = getNodes().iterator();
        Node1D lastNode = iterator.next();
        while (iterator.hasNext()) {
            Node1D current = iterator.next();
            lastNode.setNext(current);
            current.setPrevious(lastNode);
            lastNode = current;
        }

        lastNode.setNext(getNodes().get(0));
        getNodes().get(0).setPrevious(lastNode);
        recalcPositions();
    }

    private void recalcPositions() {
        Iterator<Node1D> iterator = getNodes().iterator();
        int i = 0;
        while (iterator.hasNext()) {
            Node1D node = iterator.next();
            positions.put(node, i++);
        }
    }

    @Override
    public int getPointDistance(Node1D from, Node1D to) {
        int fromPos = positions.get(from);
        int toPos = positions.get(to);
        int minPos;
        int maxPos;
        if (fromPos > toPos) {
            minPos = toPos;
            maxPos = fromPos;
        } else {
            minPos = fromPos;
            maxPos = toPos;
        }
        return Math.min(maxPos - minPos, minPos + getNodes().size() - maxPos);
    }

    @Override
    public Node1D create(double x, double y) {
        return new Node1D(x, y);
    }

    @Override
    public void pull(Point toPoint, double maxPointRadius, double learningRate) {
        super.pull(toPoint, maxPointRadius, learningRate);

//        for (Node1D node : getNodes()) {
//            for (Node1D other : getNodes()) {
//                if (node != other && node != other.getNext() && node.getNext() != other && intersects(node, node.getNext(), other, other.getNext())) {
//                    System.out.println("INTERSECT");
//                }
//            }
//        }
    }

    private static boolean intersects(Point a, Point b, Point c, Point d)
    {
        double denominator = ((b.getX() - a.getX()) * (d.getY() - c.getY())) - ((b.getY() - a.getY()) * (d.getX() - c.getX()));
        double numerator1 = ((a.getY() - c.getY()) * (d.getX() - c.getX())) - ((a.getX() - c.getX()) * (d.getY() - c.getY()));
        double numerator2 = ((a.getY() - c.getY()) * (b.getX() - a.getX())) - ((a.getX() - c.getX()) * (b.getY() - a.getY()));

        // Detect coincident lines (has a problem, read below)
        if (denominator == 0) return numerator1 == 0 && numerator2 == 0;

        double r = numerator1 / denominator;
        double s = numerator2 / denominator;

        return (r >= 0 && r <= 1) && (s >= 0 && s <= 1);
    }
}
