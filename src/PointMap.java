
import java.util.Collection;

/**
 * Created by Georg Plaz.
 */
public abstract class PointMap<A extends Node> {
    public void pull(Point toPoint, double maxPointRadius, double learningRate) {
        A closestPoint = findClosest(toPoint);
        for (A node : getNodes()) {
            int pointDistance = getPointDistance(node, closestPoint);
            double pullStrength;
            if (pointDistance == 0) {
                pullStrength = 1;
            } else {
                double denominator = (2 * maxPointRadius) * maxPointRadius;
                pullStrength = denominator > 0 ? Math.exp(-(pointDistance * pointDistance) / denominator) : 0;
            }
            node.move(toPoint, pullStrength * learningRate);
        }
    }

    public abstract Collection<A> getNodes();

    public A findClosest(Point toPoint) {
        double minDistance = Double.POSITIVE_INFINITY;
        A minPoint = null;
        for (A node : getNodes()) {
            double a = toPoint.getX() - node.getX();
            double b = toPoint.getY() - node.getY();
            double distance = a*a + b*b;
            if (distance < minDistance) {
                minDistance = distance;
                minPoint = node;
            }
        }
        return minPoint;
    }

    public abstract int getPointDistance(A from, A to);

    public void finish() {}

    public abstract void add(double nextFloat, double y);
}
