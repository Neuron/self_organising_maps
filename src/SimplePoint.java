/**
 * Created by Georg Plaz.
 */
public class SimplePoint implements Point {
    private double x;
    private double y;

    public SimplePoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }
}
