import java.awt.*;

/**
 * Created by Georg Plaz.
 */
public class Painter {
//    public List<Point> points = new LinkedList<>();
    public double minPointX = Double.POSITIVE_INFINITY;
    public double minPointY = Double.POSITIVE_INFINITY;
    public double maxPointX = Double.NEGATIVE_INFINITY;
    public double maxPointY = Double.NEGATIVE_INFINITY;
    public double scalingFactor;

    public void add(Point point) {
        minPointX = Math.min(minPointX, point.getX());
        minPointY = Math.min(minPointY, point.getY());
        maxPointX = Math.max(maxPointX, point.getX());
        maxPointY = Math.max(maxPointY, point.getY());
        scalingFactor = Math.max(maxPointX - minPointX, maxPointY - minPointY);
    }

    public void fillOval(double x, double y, double radius, Color color, double width, float height, Graphics g) {
        g.setColor(color);
        g.fillOval((int)(scaleX(x, width, height) - radius / 2),
                (int)(scaleY(y, width, height) - radius / 2),
                (int)(radius),
                (int)(radius));
    }

    public int scaleX(double x, double width, double height) {
        double scaling = getScaling(width, height);
        double normalizedX = ((x- minPointX) * scaling);
        double maxPointNormalized = (maxPointX - minPointX) * scaling;
        return (int)(normalizedX + (width-maxPointNormalized)/2);
    }

    public int scaleY(double y, double width, double height) {
        double scaling = getScaling(width, height);
        double normalizedY = (y - minPointY) * scaling;
        double maxPointNormalized = -(maxPointY - minPointY) * scaling + height;
        return (int)((-normalizedY + height - maxPointNormalized/2));
    }

    private double getScaling(double width, double height) {
        double pointsWidth = maxPointX - minPointX;
        double pointsHeight = maxPointY - minPointY;
        double xScale = width / pointsWidth;
        double yScale = height / pointsHeight;
        return Math.min(xScale, yScale);
    }

    public double getXScale() {
        return (maxPointX - minPointX) / (maxPointY - minPointY);
    }

    public void drawLine(double x, double y, double toX, double toY, Color color, double width, double height, Graphics2D g, float stroke) {
        g.setColor(color);
        g.setStroke(new BasicStroke(stroke));
        g.drawLine(scaleX(x, width, height),
                scaleY(y, width, height),
                scaleX(toX, width, height),
                scaleY(toY, width, height));
    }

    public void drawString(String s, double x, double y, int width, float height, Graphics2D g) {
        g.drawString(s, scaleX(x, width, height), scaleY(y, width, height));
    }

    public void drawOval(double x, double y, double radius, Color color, double width, float height, Graphics2D g, float stroke) {
        g.setColor(color);
        g.setStroke(new BasicStroke(stroke));
        g.drawOval((int)(scaleX(x, width, height) - radius / 2),
                (int)(scaleY(y, width, height) - radius / 2),
                (int)(radius),
                (int)(radius));
    }
}
