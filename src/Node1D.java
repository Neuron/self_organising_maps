import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public class Node1D implements Node{
    private double x;
    private double y;
    private Node1D next = null;
    private Node1D previous = null;
    private Set<Node> neighbours = new HashSet<>();

    public Node1D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setNext(Node1D next) {
        this.next = next;
        neighbours.clear();
        if (previous != null) {
            neighbours.add(next);
            neighbours.add(previous);
        }
    }

    public Node1D getNext() {
        return next;
    }

    public void setPrevious(Node1D previous) {
        this.previous = previous;
        neighbours.clear();
        if (next != null) {
            neighbours.add(next);
            neighbours.add(previous);
        }
    }

    public Node1D getPrevious() {
        return previous;
    }

    @Override
    public Set<Node> getNeighbours() {
        return neighbours;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public void setX(double x) {
        this.x = x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }

    //    private Collection<Node> neighbours = new HashSet<>();
}
