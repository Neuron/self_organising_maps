import java.awt.*;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class Solution {
    private List<Point> destinations;
    private Painter painter;
    private double distance;
    private boolean hasDistance = false;

    public Solution(List<Point> destinations, Painter painter) {
        this.destinations = destinations;
        this.painter = painter;
    }

    public double getDistance() {
        if (!hasDistance) {
            Iterator<Point> iterator = destinations.iterator();
            Point lastPoint = iterator.next();
            boolean running = true;
            while (running) {
                Point current;
                if (iterator.hasNext()) {
                    current = iterator.next();
                } else {
                    current = destinations.get(0);
                    running = false;
                }
                double dx = lastPoint.getX() - current.getX();
                double dy = lastPoint.getY() - current.getY();
                distance += Math.sqrt(dx*dx+dy*dy);
                lastPoint = current;
            }
            hasDistance = true;
        }
        return distance;
    }

    public List<Point> getDestinations() {
        return destinations;
    }


    public void visualize(Graphics2D g, int width, int height, Color colour, int stroke) {
        Iterator<Point> iterator = destinations.iterator();
        Point lastPoint = iterator.next();
        while (iterator.hasNext()) {
            Point current = iterator.next();
            painter.drawLine(current.getX(), current.getY(), lastPoint.getX(), lastPoint.getY(), colour, width, height, g, stroke);
            lastPoint = current;
        }
        painter.drawLine(destinations.get(0).getX(), destinations.get(0).getY(), lastPoint.getX(), lastPoint.getY(), colour, width, height, g, stroke);
    }
}
