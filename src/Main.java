import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Georg Plaz.
 */
public class Main {
    public static final int WIDTH = 1920;
    public static final int HEIGHT = 1080;
    public static final long MAX_MILLISECONDS = (long) (4.9 * 60 * 1000);
    public static final double MIN_LEARNING_RATE = 0.01;

    public static final boolean VISUALIZE_CURRENT_SOLUTION = false;
    public static final boolean RENDER_VIDEO = true;
    public static final boolean VISUALIZE_PROCESS = true;

    public static final int BEST_SOLUTION_STROKE = 3;
    public static final int CURRENT_SOLUTION_STROKE = 1;
    public static final int FPS = 30;
    public static final int RENDER_DELAY_MILLI = 1000/FPS;
    public static final Color BACKGROUND_COLOUR = new Color(136, 204, 238);
    public static final Color BEST_SOLUTION_COLOUR = new Color(48, 125, 37);
    public static final Color CURRENT_SOLUTION_COLOUR = new Color(8, 0, 8);
    public static final Color TEXT_COLOUR = Color.BLACK;
    public static final String RENDER_FOLDER = "rendered";
    public static final int VIDEO_EPOCH_DELAY = 100;

    public static void main(String... args) throws Exception {
        long startingTime = System.currentTimeMillis();

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File("input_E.txt"))));

        String line;
        ArrayList<Point> destinations = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            String[] splitLine = line.split(",");
            int id = Integer.parseInt(splitLine[0]);
            double x = Double.parseDouble(splitLine[1]);
            double y = Double.parseDouble(splitLine[2]);
            destinations.add(new SimplePoint(x, y));
        }
//        ArrayList<Point> normalizedDestinations = new ArrayList<>();
//        double scaling = Math.max(maxPointX - minX, maxPointY - minPointY);
//        for (Point destination : destinations) {
//            normalizedDestinations.add(new SimplePoint((destination.getX()-minX) / scaling, (destination.getY()-minPointY) / scaling));
//        }
        reader.close();
        int attemptCounter = 0;
        SOMJFrame somjFrame = new SOMJFrame(WIDTH, HEIGHT);
        if (VISUALIZE_PROCESS) {
            somjFrame.setVisible(true);
        }
        String nameBase = "output";
        int counter = 0;
        String nameExtension = ".mp4";
        File file;
        while ((file = new File(RENDER_FOLDER + "/" + nameBase + "_" + (counter) + nameExtension)).exists()) {
            counter++;
        }
        if (RENDER_VIDEO) {
            somjFrame.startRecording(file, WIDTH, HEIGHT);
        }

        Solution best = null;
        long videoTimePassed = 0;
        while (System.currentTimeMillis() - startingTime < MAX_MILLISECONDS && somjFrame.isDisplayable()) {
            somjFrame.getPanel().setAttemptCounter(attemptCounter);
            SelfOrganizingMap selfOrganizingMap = new SelfOrganizingMap(destinations, new PointCircle(), (int) (destinations.size() * 2.));
            somjFrame.getPanel().setSelfOrganizingMap(selfOrganizingMap);
            if (VISUALIZE_PROCESS) {
                somjFrame.getPanel().repaint();
            }
            long nextRenderTime = System.currentTimeMillis();
            long nextTickVideoRender = 0;
            while (selfOrganizingMap.getLearningRate() > MIN_LEARNING_RATE &&
                    System.currentTimeMillis() - startingTime < MAX_MILLISECONDS &&
                    somjFrame.isDisplayable()) {
                long currentTime = System.currentTimeMillis();
                if (currentTime >= nextRenderTime) {
                    if (VISUALIZE_CURRENT_SOLUTION) {
                        Solution currentSolution = selfOrganizingMap.getSolution();
                        somjFrame.getPanel().setCurrentSolution(currentSolution);
                    }
                    if (VISUALIZE_PROCESS) {
                        somjFrame.getPanel().repaint();
                    }
                    nextRenderTime = currentTime + RENDER_DELAY_MILLI;
                }
                if (RENDER_VIDEO) {
                    if (selfOrganizingMap.getTickCount() >= nextTickVideoRender) {
                        System.out.println("rendering to video "+videoTimePassed);
                        somjFrame.captureImage(videoTimePassed, TimeUnit.MILLISECONDS);
                        nextTickVideoRender += VIDEO_EPOCH_DELAY;
                        videoTimePassed += RENDER_DELAY_MILLI;
                    }
                }
                selfOrganizingMap.tick();
            }

            Solution currentSolution = selfOrganizingMap.getSolution();
            if (best == null || best.getDistance() > currentSolution.getDistance()) {
                best = currentSolution;
                somjFrame.getPanel().setBestSolution(best);
                System.out.println("found new solution with distance "+best.getDistance());
            }
            somjFrame.getPanel().repaint();
            attemptCounter++;
        }
        System.out.println("finished");
        somjFrame.stopRecording();
    }

}
