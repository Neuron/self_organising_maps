import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class SelfOrganizingMap {
    public static final double DESTINATION_RADIUS = 9;
    public static final Color DESTINATION_OUTER_COLOUR = new Color(51, 34, 136);
//    public static final Color DESTINATION_INNER_COLOUR = new Color(51, 34, 136);
    public static final double NODE_RADIUS = 7;
    public static final Color NODE_COLOUR = new Color(1, 40, 48);

    private ArrayList<Point> destinations;
    private PointMap<?> pointMap;
    private Random random;
    private double pointRadius;
    private double learningRate;
    private int tickCount;
    private Painter painter = new Painter();

    public SelfOrganizingMap(ArrayList<Point> destinations, PointMap<?> pointMap, int pointCount) {
        this.destinations = new ArrayList<>(destinations);
        this.pointMap = pointMap;
        random = new Random();

        pointRadius = (destinations.size() - 1)/2;
        learningRate = 0.2;
        tickCount = 0;

        for (int i = 0; i < pointCount; i++) {
            pointMap.add(random.nextFloat(), random.nextFloat());
        }
        pointMap.finish();
        
        for (Point destination : destinations) {
            painter.add(destination);
        }
    }

    public void visualize(Graphics2D g, int width, float height) {
        int i = 0;
        for (Point destination : destinations) {
            painter.drawOval(destination.getX(), destination.getY(), DESTINATION_RADIUS, DESTINATION_OUTER_COLOUR, width, height, g, 3);
            painter.drawString(String.valueOf(i), destination.getX(), destination.getY(), width, height, g);
            i++;
        }
        i = 0;
        for (Node node : pointMap.getNodes()) {
            painter.fillOval(node.getX(), node.getY(), NODE_RADIUS, NODE_COLOUR, width, height, g);
//            g.drawString(String.valueOf(i), (int)(node.getX()*scaleX), (int)(node.getY()*scaleY));
            i++;
            for (Point neighbour : node.getNeighbours()) {
                painter.drawLine(node.getX(), node.getY(), neighbour.getX(), neighbour.getY(), Color.BLACK, width, height, g, 1);
            }
        }
    }

    public void tick() {
        learningRate = 1 * Math.exp(-(tickCount / 2000.));
        pointRadius = 0.5 * pointMap.getNodes().size() * Math.exp(-(tickCount / 500.));
//        pointRadius = pointRadius*0.9;//
        tickCount++;
        Point destination = destinations.get(random.nextInt(destinations.size()));
        pointMap.pull(destination, pointRadius, learningRate);
    }

    public int getTickCount() {
        return tickCount;
    }

    public double getPointRadius() {
        return pointRadius;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public Solution getSolution() {
        List<Point> solutionList = new LinkedList<>();
        Map<Node, List<Point>> map = new HashMap<>();
        for (Point destination : destinations) {
            Node closest = pointMap.findClosest(destination);
            if (!map.containsKey(closest)) {
                map.put(closest, new LinkedList<>());
            }
            map.get(closest).add(destination);
        }
        for (Node node : pointMap.getNodes()) {
            if (map.containsKey(node)) {
                for (Point destination : map.get(node)) {
                    solutionList.add(destination);
                }
            }
        }
        return new Solution(solutionList, painter);
    }

    public Painter getPainter() {
        return painter;
    }
}
