import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public interface Node extends Point {
    Set<Node> getNeighbours();

    void setX(double x);

    void setY(double y);

    default void move(Point toPoint, double scaling) {
        double dx = (toPoint.getX() - getX()) * scaling;
        double dy = (toPoint.getY() - getY()) * scaling;
        move(dx, dy);
    }

    default void move(double dx, double dy) {
        setX(getX()+dx);
        setY(getY()+dy);
    }
}
