/**
 * Created by Georg Plaz.
 */
public interface Point {
    double getX();
    double getY();
}
