import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by Georg Plaz.
 */
public class SOMJFrame extends VideoFrame{
    private final SOMPanel panel;

    public SOMJFrame(int width, int height) {
        super("Self Organising Map");
        panel = new SOMPanel();
        panel.setPreferredSize(new Dimension(width, height));
        panel.setStartingTime(System.currentTimeMillis());
        add(panel);
        pack();

    }

    public SOMPanel getPanel() {
        return panel;
    }

    @Override
    public void doDispose() {
        super.doDispose();
//        System.exit(0);
    }
}
