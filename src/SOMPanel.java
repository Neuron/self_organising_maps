import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Georg Plaz.
 */
class SOMPanel extends JPanel {
    public static final int LINE_DELTA = 15;
    private SelfOrganizingMap selfOrganizingMap;
    private Solution bestSolution = null;
    private Solution currentSolution = null;
    private int attemptCounter = -1;
    private long startingTime;

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D graphics = (Graphics2D) g;
        graphics.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        int linePosition = 0;
        graphics.setColor(Main.BACKGROUND_COLOUR);
        graphics.fillRect(0, 0, getWidth(), getHeight());

        if (selfOrganizingMap != null) {
            graphics.setColor(Main.TEXT_COLOUR);
            graphics.drawString("attempt: " + attemptCounter, 5, linePosition += LINE_DELTA);

//            graphics.setColor(Main.TEXT_COLOUR);
//            graphics.drawString("time: " + (System.currentTimeMillis() - startingTime) / 1000., 5, linePosition += LINE_DELTA);

            graphics.setColor(Main.TEXT_COLOUR);
            graphics.drawString("epoch: " + selfOrganizingMap.getTickCount(), 5, linePosition += LINE_DELTA);

            graphics.setColor(Main.TEXT_COLOUR);
            graphics.drawString("learning rate: " + selfOrganizingMap.getLearningRate(), 5, linePosition += LINE_DELTA);

            if (bestSolution != null) {
                bestSolution.visualize(graphics, (int) (getPreferredSize().getWidth()), (int) (getPreferredSize().getHeight()), Main.BEST_SOLUTION_COLOUR, Main.BEST_SOLUTION_STROKE);
            }
            if (currentSolution != null) {
                currentSolution.visualize(graphics, (int) (getPreferredSize().getWidth()), (int) (getPreferredSize().getHeight()), Main.CURRENT_SOLUTION_COLOUR, Main.CURRENT_SOLUTION_STROKE);
                graphics.setColor(Main.TEXT_COLOUR);
                graphics.drawString("current solution length: " + currentSolution.getDistance(), 5, linePosition += LINE_DELTA);
            }

            if (bestSolution != null) {
                graphics.setColor(Main.TEXT_COLOUR);
                graphics.drawString("best solution length: " + bestSolution.getDistance(), 5, linePosition += LINE_DELTA);
            }
            selfOrganizingMap.visualize(graphics, (int) (getPreferredSize().getWidth()), (int) (getPreferredSize().getHeight()));
        }
    }


    public void setSelfOrganizingMap(SelfOrganizingMap selfOrganizingMap) {
        this.selfOrganizingMap = selfOrganizingMap;
    }

    public void setBestSolution(Solution bestSolution) {
        this.bestSolution = bestSolution;
    }

    public void setCurrentSolution(Solution currentSolution) {
        this.currentSolution = currentSolution;
    }

    public void setAttemptCounter(int attemptCounter) {
        this.attemptCounter = attemptCounter;
    }

    public void setStartingTime(long startingTime) {
        this.startingTime = startingTime;
    }

    //        public void setP(int width) {
//            this.width = width;
//        }
//
//        public void setHeight(int height) {
//            this.height = height;
//        }
}
