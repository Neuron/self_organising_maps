import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Collection;

/**
 * Created by Georg Plaz.
 */
public class PointLine extends PointMap1D<Node1D> {
    @Override
    public int getPointDistance(Node1D from, Node1D to) {
        throw new NotImplementedException();
    }

    @Override
    public Node1D create(double x, double y) {
        return new Node1D(x, y);
    }
}
